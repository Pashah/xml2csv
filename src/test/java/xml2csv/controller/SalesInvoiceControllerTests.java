package xml2csv.controller;

import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import xml2csv.Xml2csvApplication;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Xml2csvApplication.class)
@WebAppConfiguration
public class SalesInvoiceControllerTests extends AbstractTransactionalJUnit4SpringContextTests {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext wac;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void salesInvoiceToCsvTest() throws Exception {
        List<String> resourceLines = IOUtils.readLines(wac.getResource("classpath:esimerkkilasku.xml").getInputStream());
        String xml = StringUtils.join(resourceLines.toArray());
        ResultActions result = mockMvc.perform(post("/convertXML2CSV")
                .contentType(MediaType.APPLICATION_XML)
                .content(xml))
                .andExpect(status().isOk());
        String content = result.andReturn().getResponse().getContentAsString();
        assertEquals("M;EUR;;;0836922-4;;Procountor International Oy;;;;;10,5;29.01.2015;;;;Procountor International Oy\\Keilaranta 8\\02150\\ESPOO\\FI;Procountor International Oy\\Keilaranta 8\\02150\\ESPOO\\FI;Asennuskulut veloitetaan erillisellä laskulla;;;;;;;2;\r\n" +
                ";Jura X9000 kahvikone;X9000;1;kpl;2000,00;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SA2;1000;kpl;0,15;;24;", content);

        //Test with 10 invoice rows
        resourceLines = IOUtils.readLines(wac.getResource("classpath:invoice_row_test_file.xml").getInputStream());
        xml = StringUtils.join(resourceLines.toArray());
        result = mockMvc.perform(post("/convertXML2CSV")
                .contentType(MediaType.APPLICATION_XML)
                .content(xml))
                .andExpect(status().isOk());
        content = result.andReturn().getResponse().getContentAsString();
        assertEquals("M;EUR;;;0836922-4;;Procountor International Oy;;;;;10,5;29.01.2015;;;;Procountor International Oy\\Keilaranta 8\\02150\\ESPOO\\FI;Procountor International Oy\\Keilaranta 8\\02150\\ESPOO\\FI;Asennuskulut veloitetaan erillisellä laskulla;;;;;;;2;\r\n" +
                ";Jura X9000 kahvikone;X9000;1;kpl;2000,00;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SA2;1000;kpl;0,15;;24;\r\n" +
                ";Jura X9100 kahvikone;X9100;1;kpl;2500,00;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SA3;1000;kpl;0,15;;24;\r\n" +
                ";Jura X100 kahvikone;X100;1;kpl;500,00;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SA4;1000;kpl;0,30;;24;\r\n" +
                ";Jura X500 kahvikone;X500;1;kpl;15;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SA;4500;kpl;0,35;;24;\r\n" +
                ";Jura XTEST kahvikone;XTEST;1;kpl;15;;24;\r\n" +
                ";Premium Saula Cafe espressonapit;SATEST;4500;kpl;0,35;;24;", content);
    }

    @Test
    public void schemaExceptionTest() {
        try {
            List<String> resourceLines = IOUtils.readLines(wac.getResource("classpath:schema_breaking.xml").getInputStream());
            String xml = StringUtils.join(resourceLines.toArray());
            ResultActions result = mockMvc.perform(post("/convertXML2CSV")
                    .contentType(MediaType.APPLICATION_XML)
                    .content(xml))
                    .andExpect(status().is5xxServerError());
            fail("Application didn't throw Exception even though input XML's schema was wrong!");
        } catch (Exception e) {
            assertEquals("Handler processing failed; nested exception is java.lang.InternalError: XML Schema error: " +
                    "cvc-complex-type.2.4.a: Invalid content was found starting with element 'InvoiceDetails'. " +
                    "One of '{MessageTransmissionDetails, SellerOrganisationUnitNumber, SellerSiteCode, " +
                    "SellerContactPersonName, SellerContactPersonFunction, SellerContactPersonDepartment, " +
                    "SellerCommunicationDetails, SellerInformationDetails, InvoiceSenderPartyDetails, " +
                    "InvoiceRecipientPartyDetails, InvoiceRecipientOrganisationUnitNumber, InvoiceRecipientSiteCode, " +
                    "InvoiceRecipientContactPersonName, InvoiceRecipientContactPersonFunction, " +
                    "InvoiceRecipientContactPersonDepartment, InvoiceRecipientLanguageCode, " +
                    "InvoiceRecipientCommunicationDetails, BuyerPartyDetails}' is expected.", e.getMessage());
        }
    }


}
