package xml2csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xml2csvApplication {

    public static void main(String[] args) {
        SpringApplication.run(Xml2csvApplication.class, args);
    }
}
