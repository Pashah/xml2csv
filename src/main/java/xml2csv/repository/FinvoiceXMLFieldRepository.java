package xml2csv.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import xml2csv.model.FinvoiceXMLField;

import java.util.List;

@RepositoryRestResource
public interface FinvoiceXMLFieldRepository extends PagingAndSortingRepository<FinvoiceXMLField, Long> {

    @Query("select f.fieldName from Finvoice_XML_Field f")
    List<String> findAllFields();

}
