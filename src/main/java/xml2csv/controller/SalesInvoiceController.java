package xml2csv.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import xml2csv.model.CSVSalesInvoice;
import xml2csv.model.InvoiceRow;
import xml2csv.service.SalesInvoiceService;

import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

@Controller
public class SalesInvoiceController {

    @Autowired
    private SalesInvoiceService salesInvoiceService;

    /**
     * Converts String xml parameter to CSV. Use this with curl or something similar.
     * For GUI use convertXMLFile2CSV
     * @param xmlSalesInvoice XmlSalesInvoice in String format
     * @param response response that is given to user
     * @throws Exception Exceptions are handled in ExceptionsHandlers
     */
    @RequestMapping(method= RequestMethod.POST, value="/convertXML2CSV", headers="Accept=application/xml")
    public void convertXML2CSV(@RequestBody String xmlSalesInvoice, HttpServletResponse response) throws Exception {
        CSVSalesInvoice csvSalesInvoice = salesInvoiceService.createSalesInvoiceFromXML(xmlSalesInvoice);
        writeCsv(response, csvSalesInvoice);
    }

    /**
     * Converts a xml file to csv file. This is used from web gui.
     * @param file file that is converted. Required.
     * @param fileName optional filename. If no name is set salesInvoice.csv will be used.
     * @param response response that is given to user
     * @param attributes used for showing error messages to the user
     * @return
     */
    @RequestMapping(method= RequestMethod.POST, value="/convertXMLFile2CSV", headers="Accept=application/xml")
    public String convertXMLFile2CSV(@RequestParam("xmlSalesInvoice") MultipartFile file, @RequestParam("fileName") String fileName,
                                     HttpServletResponse response, RedirectAttributes attributes) {
        if(file.isEmpty()) {
            attributes.addFlashAttribute("message", "Error: File is empty!");
            return "redirect:/";
        }
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(file.getInputStream(), writer);
            String xmlSalesInvoice = writer.toString();
            CSVSalesInvoice csvSalesInvoice = salesInvoiceService.createSalesInvoiceFromXML(xmlSalesInvoice);
            if(fileName.isEmpty())
                fileName = "salesInvoice";
            String csvFileName = fileName + ".csv";
            response.setContentType("text/csv");
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",csvFileName);
            response.setHeader(headerKey, headerValue);
            writeCsv(response, csvSalesInvoice);
            return null;
        } catch (Exception e) {
            attributes.addFlashAttribute("message", "Error: XML2CSV converting failed because of: " + e.getMessage());
        }
        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.GET, value="/")
    public String getMainpage(Model model) {
        return "mainpage";
    }

    /**
     * Writes CSV File to user from CSVSalesInvoice object
     * @param response response that is given to user
     * @param csvSalesInvoice CSVSalesInvoice that has all CSV data inside formatted
     * @throws Exception
     */
    private void writeCsv(HttpServletResponse response, CSVSalesInvoice csvSalesInvoice) throws Exception {
        PrintWriter printWriter = response.getWriter();
        printWriter.print(csvSalesInvoice.getHeaderData());
        for (InvoiceRow invoiceRow : csvSalesInvoice.getInvoiceRows()) {
            printWriter.print(System.lineSeparator());
            printWriter.print(invoiceRow.getCSVData());
        }
        printWriter.close();
    }

    /**
     * Error handling for XMLStreamException.
     * @param exception
     * @return
     */
    @ExceptionHandler(XMLStreamException.class)
    public String databaseError(XMLStreamException exception) {
        throw new InternalError("XML Stream error when parsing XML file: " + exception.getMessage());
    }

    /**
     * Error handling for SAXException
     * @param exception
     * @return
     */
    @ExceptionHandler(SAXException.class)
    public String xmlSchemaException(SAXException exception) {
        throw new InternalError("XML Schema error: " + exception.getMessage());
    }

    /**
     * Error handling for IOException
     * @param exception
     * @return
     */
    @ExceptionHandler(IOException.class)
    public String xmlSchemaException(IOException exception) {
        throw new InternalError("IOException: " + exception.getMessage());
    }

}
