package xml2csv.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Finvoice_XML_Field")
public class FinvoiceXMLField {

    @Id
    private long id;
    private String fieldName;

}
