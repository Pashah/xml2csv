package xml2csv.model;

import java.util.Map;

public class InvoiceDetails {

    private String paymentOverDueFinePercent;
    private String invoiceDate;
    private String freeText;
    private String invoiceTypeCode;

    public InvoiceDetails(Map<String, String> map) {
        this.paymentOverDueFinePercent = map.get("PaymentOverDueFinePercent");
        this.invoiceDate = map.get("InvoiceDate");
        this.freeText = map.get("InvoiceFreeText");
        this.invoiceTypeCode = map.get("InvoiceTypeCode");
    }

    public String getPaymentOverDueFinePercent() {
        return paymentOverDueFinePercent;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getFreeText() {
        return freeText;
    }

    public String getInvoiceTypeCode() {
        return invoiceTypeCode;
    }
}
