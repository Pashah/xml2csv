package xml2csv.model;

public class InvoiceRow {

    private String articleIdentifier;
    private String articleName;
    private int orderedQuantity;
    private String unitPriceAmount;
    private String rowVatRatePercent;
    private String unitPriceUnitCode;
    private String amountCurrencyIdentifier;

    public void setArticleIdentifier(String articleIdentifier) {
        this.articleIdentifier = articleIdentifier;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public void setUnitPriceAmount(String unitPriceAmount) {
        this.unitPriceAmount = unitPriceAmount;
    }

    public void setRowVatRatePercent(String rowVatRatePercent) {
        this.rowVatRatePercent = rowVatRatePercent;
    }

    public void setUnitPriceUnitCode(String unitPriceUnitCode) {
        this.unitPriceUnitCode = unitPriceUnitCode;
    }

    public String getAmountCurrencyIdentifier() {
        return amountCurrencyIdentifier;
    }

    public void setAmountCurrencyIdentifier(String amountCurrencyIdentifier) {
        this.amountCurrencyIdentifier = amountCurrencyIdentifier;
    }

    /**
     * Parses together Finvoice CSV data from this object to an InvoiceRow in csv document
     * CSV data format:
     * null;ArticleName;ArticleIdentifier;OrderedQuantity;UnitPriceUnitCode;unitPriceAmount;null;RowVatRatePercent;
     * Uses String concanation instead of StringBuilder because JVM does optimization of this anyway and it's
     * more readable with just String.
     * @return Finvoice CSVData
     */
    public String getCSVData() {
        return  ";" + this.articleName + ";" + this.articleIdentifier + ";" + this.orderedQuantity +
                ";" + this.unitPriceUnitCode + ";" + this.unitPriceAmount + ";;" + this.rowVatRatePercent + ";";
    }
}
