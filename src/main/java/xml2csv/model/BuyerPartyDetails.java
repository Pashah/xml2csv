package xml2csv.model;

import xml2csv.helper.ProcountorPostalAddressHelper;

import java.util.Map;

public class BuyerPartyDetails {

    private String buyerPartyIdentifier;
    private String buyerOrganisationName;
    private PostalAddressDetails postalAddressDetails;

    public BuyerPartyDetails(Map<String, String> map) {
        this.buyerPartyIdentifier = map.get("BuyerPartyIdentifier");
        this.buyerOrganisationName = map.get("BuyerOrganisationName");
        this.postalAddressDetails = new PostalAddressDetails(map);
    }


    public String getBuyerPartyIdentifier() {
        return buyerPartyIdentifier;
    }

    public PostalAddressDetails getPostalAddressDetails() {
        return postalAddressDetails;
    }

    public String getProcountorPostalDetails() {
        PostalAddressDetails postalAddressDetails = this.getPostalAddressDetails();
        return this.buyerOrganisationName + "\\" +
                ProcountorPostalAddressHelper.parseProcountorPostalDetails(postalAddressDetails);
    }

}
