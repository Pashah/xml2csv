package xml2csv.model;

import java.util.List;
import java.util.Map;

public class CSVSalesInvoice {

    private DeliveryPartyDetails deliveryPartyDetails;
    private BuyerPartyDetails buyerPartyDetails;
    private InvoiceDetails invoiceDetails;
    private List<InvoiceRow> invoiceRows;


    public CSVSalesInvoice(Map<String, String> map, List<InvoiceRow> invoiceRowList) {
        parseDataFromMap(map);
        this.invoiceRows = invoiceRowList;
    }

    private void parseDataFromMap(Map<String, String> map) {
        this.invoiceDetails = new InvoiceDetails(map);
        this.buyerPartyDetails = new BuyerPartyDetails(map);
        this.deliveryPartyDetails = new DeliveryPartyDetails(map);
    }

    public List<InvoiceRow> getInvoiceRows() {
        return invoiceRows;
    }

    //Parses CSV header data from invoiceDetails, invoiceRows and buyerPartyDetails
    //String concatenation done without StringBuilder because
    //JVM optimizes it
    public String getHeaderData() {
        return this.invoiceDetails.getInvoiceTypeCode() + ";" + this.invoiceRows.get(0).getAmountCurrencyIdentifier()
                + ";;;" + buyerPartyDetails.getBuyerPartyIdentifier() + ";;" + deliveryPartyDetails.getDeliveryOrganisationName()
                + ";;;;;" + invoiceDetails.getPaymentOverDueFinePercent() + ";" + parseProcountorDate(invoiceDetails.getInvoiceDate())
                + ";;;;" + deliveryPartyDetails.getProcountorPostalDetails() + ";" + buyerPartyDetails.getProcountorPostalDetails()
                + ";" + invoiceDetails.getFreeText() + ";;;;;;;2;";
    }

    private String parseProcountorDate(String xmlDate) {
        return xmlDate.substring(6, 8) + "." + xmlDate.substring(4, 6) + "." + xmlDate.substring(0, 4);
    }

}
