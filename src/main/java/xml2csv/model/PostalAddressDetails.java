package xml2csv.model;

import java.util.Map;

public class PostalAddressDetails {

    private String streetName;
    private String townName;
    private String postCode;
    private String countryCode;

    public PostalAddressDetails(Map<String, String> map) {
        this.streetName = map.get("BuyerStreetName");
        this.townName = map.get("BuyerTownName");
        this.postCode = map.get("BuyerPostCodeIdentifier");
        this.countryCode = map.get("CountryCode");
    }

    public String getStreetName() {
        return streetName;
    }

    public String getTownName() {
        return townName;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

}
