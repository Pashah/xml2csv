package xml2csv.model;

import xml2csv.helper.ProcountorPostalAddressHelper;

import java.util.Map;

public class DeliveryPartyDetails {

    private String deliveryOrganisationName;
    private PostalAddressDetails postalAddressDetails;

    public DeliveryPartyDetails(Map<String, String> map) {
        this.deliveryOrganisationName = map.get("DeliveryOrganisationName");
        this.postalAddressDetails = new PostalAddressDetails(map);
    }

    public String getDeliveryOrganisationName() {
        return deliveryOrganisationName;
    }

    public PostalAddressDetails getPostalAddressDetails() {
        return postalAddressDetails;
    }

    public String getProcountorPostalDetails() {
        PostalAddressDetails postalAddressDetails = this.getPostalAddressDetails();
        return this.deliveryOrganisationName + "\\" +
                ProcountorPostalAddressHelper.parseProcountorPostalDetails(postalAddressDetails);
    }
}
