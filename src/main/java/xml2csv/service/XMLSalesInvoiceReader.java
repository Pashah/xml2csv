package xml2csv.service;

import xml2csv.model.*;
import org.apache.commons.io.IOUtils;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class XMLSalesInvoiceReader {

    /**
     * Parses XMLSalesInvoice to CSVSalesInvoice. Does the actual parsing. If there is need to create new XML formats
     * here is code that could be reused. Such as putting data to the map.
     * @param xmlSalesInvoice xml string which is parsed to csv
     * @param xmlFields contains xmlFields with empty values. Values are added during parsing
     * @return returns CSVSalesInvoice with all needed info
     * @throws XMLStreamException
     */
    public CSVSalesInvoice parseXMLSalesInvoice(String xmlSalesInvoice, Map<String, String> xmlFields) throws XMLStreamException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(IOUtils.toInputStream(xmlSalesInvoice));
        List<InvoiceRow> invoiceRowList = new ArrayList<InvoiceRow>();
        InvoiceRow invoiceRow = null;
        while(xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if(xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                String localName = startElement.getName().getLocalPart();
                if(xmlFields.get(localName) != null) {
                    xmlEvent = xmlEventReader.nextEvent();
                    xmlFields.put(localName, xmlEvent.asCharacters().getData());
                } else if(localName.equals("InvoiceRow")) {
                    invoiceRow = new InvoiceRow();
                } else {
                    parseInvoiceRows(xmlEventReader.nextEvent(), localName, invoiceRow, startElement);
                }
            }
            if(xmlEvent.isEndElement())
                parseEndElements(xmlEvent, invoiceRowList, invoiceRow);
        }
        return new CSVSalesInvoice(xmlFields, invoiceRowList);
    }

    private void parseInvoiceRows(XMLEvent xmlEvent, String localName, InvoiceRow invoiceRow, StartElement startElement) {
        if(localName.equals("ArticleIdentifier"))
            invoiceRow.setArticleIdentifier(xmlEvent.asCharacters().getData());
        else if(localName.equals("ArticleName"))
            invoiceRow.setArticleName(xmlEvent.asCharacters().getData());
        else if(localName.equals("OrderedQuantity"))
            invoiceRow.setOrderedQuantity(Integer.parseInt(xmlEvent.asCharacters().getData()));
        else if(localName.equals("UnitPriceAmount")) {
            invoiceRow.setUnitPriceAmount((xmlEvent.asCharacters().getData()));
            invoiceRow.setUnitPriceUnitCode(getAttributeValue(startElement, "UnitPriceUnitCode"));
            invoiceRow.setAmountCurrencyIdentifier(getAttributeValue(startElement, "AmountCurrencyIdentifier"));
        } else if(localName.equals("RowVatRatePercent"))
            invoiceRow.setRowVatRatePercent(xmlEvent.asCharacters().getData());
    }

    private String getAttributeValue(StartElement startElement, String attributeName) {
        QName qName = new QName(null, attributeName);
        Attribute attribute = startElement.getAttributeByName(qName);
        return attribute.getValue();
    }

    private void parseEndElements(XMLEvent xmlEvent, List<InvoiceRow> invoiceRowList, InvoiceRow invoiceRow) {
        EndElement endElement = xmlEvent.asEndElement();
        if(endElement.getName().getLocalPart().equals("InvoiceRow"))
            invoiceRowList.add(invoiceRow);
    }

}
