package xml2csv.service;

import xml2csv.model.CSVSalesInvoice;

public interface SalesInvoiceService {

    CSVSalesInvoice createSalesInvoiceFromXML(String xmlSalesInvoice) throws Exception;
}
