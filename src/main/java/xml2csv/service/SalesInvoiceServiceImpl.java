package xml2csv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import xml2csv.model.CSVSalesInvoice;
import xml2csv.repository.FinvoiceXMLFieldRepository;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SalesInvoiceServiceImpl implements SalesInvoiceService {

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private FinvoiceXMLFieldRepository finvoiceXMLFieldRepository;


    @Override
    public CSVSalesInvoice createSalesInvoiceFromXML(String xmlSalesInvoice) throws Exception {
        validateXML(xmlSalesInvoice);
        Map<String, String> xmlFields = getXMLFields("finvoice");
        CSVSalesInvoice csvSalesInvoice = parseXMLSalesInvoice(xmlSalesInvoice, xmlFields);
        return csvSalesInvoice;
    }

    /**
     * Validates XML file with xsd schema file. Throws SAXException if there are errors.
     * @param xmlString xml string that is validated
     * @return returns true if xml is valid otherwise throws SAXException
     * @throws IOException thrown if xsd file is not found from classpath
     * @throws SAXException thrown if xml file is invalid
     */
    private boolean validateXML(String xmlString) throws IOException, SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(ctx.getResource("classpath:finvoice.xsd").getFile());
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new StringReader(xmlString)));
        return true;
    }

    /**
     * Parses XML fields to CSVSalesInvoice which is later used to construct CSV file.
     * @param xmlSalesInvoice xml String where fields are parsed from
     * @param xmlFields name of fields
     * @return CSVSalesInvoice which is used to construct CSV file
     * @throws XMLStreamException
     */
    private CSVSalesInvoice parseXMLSalesInvoice(String xmlSalesInvoice, Map<String, String> xmlFields) throws XMLStreamException {
        XMLSalesInvoiceReader xmlSalesInvoiceReader = new XMLSalesInvoiceReader();
        return xmlSalesInvoiceReader.parseXMLSalesInvoice(xmlSalesInvoice, xmlFields);
    }

    /**
     * Gets XML fields from db. Currently on finvoice is implemented.
     */
    private Map<String, String> getXMLFields(String xmlFileType) {
        if(xmlFileType.equals("finvoice"))
            return createMapFromListOfKeys(finvoiceXMLFieldRepository.findAllFields());
        return null;
    }

    /**
     * Creates map of keys with empty values from a list of keys.
     */
    private Map<String, String> createMapFromListOfKeys(List<String> keys) {
        Map<String, String> map = new HashMap<String, String>();
        for (String key : keys) {
            map.put(key,"");
        }
        return map;
    }
}
