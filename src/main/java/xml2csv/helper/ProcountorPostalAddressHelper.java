package xml2csv.helper;

import xml2csv.model.PostalAddressDetails;

public class ProcountorPostalAddressHelper {


    /**
     * Converts postalAddressDetails to Procountor format which is:
     * streetName\postCode\townName\CountryCode
     * @param postalAddressDetails if null returns empty string
     * @return postalAddressDetail in Procountor format
     */
    public static String parseProcountorPostalDetails(PostalAddressDetails postalAddressDetails) {
        if(postalAddressDetails == null)
            return "";
        return postalAddressDetails.getStreetName() + "\\" + postalAddressDetails.getPostCode() + "\\" +
                postalAddressDetails.getTownName() + "\\" + postalAddressDetails.getCountryCode();
    }

}
